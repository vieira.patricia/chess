package com.sensys.gatso.chess.enumerator;

public enum ChessException {

	NOT_ALLOWED("MOVEMENT_NOT ALLOWED", "MOVEMENT_NOT ALLOWED");

	String key;
	String message;

	ChessException(String key, String message) {
		this.key = key;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public String getKey() {
		return key;
	}
}
