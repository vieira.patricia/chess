package com.sensys.gatso.chess.enumerator;



import com.sensys.gatso.chess.domain.Position;
import com.sensys.gatso.chess.strategy.BishopMovement;
import com.sensys.gatso.chess.strategy.ChessMovementStrategy;
import com.sensys.gatso.chess.strategy.KingMovement;
import com.sensys.gatso.chess.strategy.KnightMovement;
import com.sensys.gatso.chess.strategy.PawnMovement;
import com.sensys.gatso.chess.strategy.QueenMovement;
import com.sensys.gatso.chess.strategy.RookMovement;

//Enum to set the pieces with initial position and "movement" to be executed dynamically by the application
public enum ChessPiece {

	//Set white pieces position on the board
	ROOK1_WHITE(Color.WHITE, new Position(0,0), new RookMovement()),
	KNIGHT1_WHITE(Color.WHITE, new Position(1,0), new KnightMovement()),
	BISHOP1_WHITE(Color.WHITE, new Position(2,0), new BishopMovement()),	
	KING_WHITE(Color.WHITE, new Position(3,0), new KingMovement()),
	QUEEN_WHITE(Color.WHITE, new Position(4,0), new QueenMovement()),
	BISHOP2_WHITE(Color.WHITE, new Position(5,0), new BishopMovement()),
	KNIGHT2_WHITE(Color.WHITE, new Position(6,0), new KnightMovement()),
	ROOK2_WHITE(Color.WHITE, new Position(7,0), new RookMovement()),		
	PAWN1_WHITE(Color.WHITE, new Position(0,1), new PawnMovement()),
	PAWN2_WHITE(Color.WHITE, new Position(1,1), new PawnMovement()),
	PAWN3_WHITE(Color.WHITE, new Position(2,1), new PawnMovement()),
	PAWN4_WHITE(Color.WHITE, new Position(3,1), new PawnMovement()),
	PAWN5_WHITE(Color.WHITE, new Position(4,1), new PawnMovement()),
	PAWN6_WHITE(Color.WHITE, new Position(5,1), new PawnMovement()),
	PAWN7_WHITE(Color.WHITE, new Position(6,1), new PawnMovement()),
	PAWN8_WHITE(Color.WHITE, new Position(7,1), new PawnMovement()),

	//Black pieces position on the board
	ROOK1_BLACK(Color.BLACK, new Position(0,7), new RookMovement()),
	KNIGHT1_BLACK(Color.BLACK, new Position(1,7), new KnightMovement()),
	BISHOP1_BLACK(Color.BLACK, new Position(2,7), new BishopMovement()),	
	KING_BLACK(Color.BLACK, new Position(3,7), new KingMovement()),
	QUEEN_BLACK(Color.BLACK, new Position(4,7), new QueenMovement()),
	BISHOP2_BLACK(Color.BLACK, new Position(5,7), new BishopMovement()),
	KNIGHT2_BLACK(Color.BLACK, new Position(6,7), new KnightMovement()),
	ROOK2_BLACK(Color.BLACK, new Position(7,7), new RookMovement()),		
	PAWN1_BLACK(Color.BLACK, new Position(0,6), new PawnMovement()),
	PAWN2_BLACK(Color.BLACK, new Position(1,6), new PawnMovement()),
	PAWN3_BLACK(Color.BLACK, new Position(2,6), new PawnMovement()),
	PAWN4_BLACK(Color.BLACK, new Position(3,6), new PawnMovement()),
	PAWN5_BLACK(Color.BLACK, new Position(4,6), new PawnMovement()),
	PAWN6_BLACK(Color.BLACK, new Position(5,6), new PawnMovement()),
	PAWN7_BLACK(Color.BLACK, new Position(6,6), new PawnMovement()),
	PAWN8_BLACK(Color.BLACK, new Position(7,6), new PawnMovement());

	private Color color;
	private Position initialPosition;
	private ChessMovementStrategy movement;

	private ChessPiece(Color color, Position initialPosition,  ChessMovementStrategy movement){
		this.color = color;
		this.initialPosition = initialPosition;
		this.movement = movement;

	}

	public Color getColor() {
		return color;
	}

	public Position getInitialPosition() {
		return initialPosition;
	}

	public ChessMovementStrategy getMovement() {
		return movement;
	}	

}
