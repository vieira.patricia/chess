package com.sensys.gatso.chess.enumerator;

public enum Color {
	
	BLACK("black","#00000"),
	WHITE("white","#11111");	
	
	private String name;
	private String htmlCode;
	
	private Color(String name, String htmlCode){
		this.name = name;
		this.htmlCode = htmlCode;
	}

	public String getName() {
		return name;
	}

	public String getHtmlCode() {
		return htmlCode;
	}
	

}
