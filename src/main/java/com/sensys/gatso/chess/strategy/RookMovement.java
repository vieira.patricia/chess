package com.sensys.gatso.chess.strategy;

import com.sensys.gatso.chess.domain.Position;
import com.sensys.gatso.chess.enumerator.ChessException;
import com.sensys.gatso.chess.exception.ChessCustomException;

public class RookMovement implements ChessMovementStrategy{

	@Override
	public Position move(Position currentPosition, Position targetPosition) throws ChessCustomException {
		
		if (!validateMovement(currentPosition, targetPosition)){
			throw new ChessCustomException(ChessException.NOT_ALLOWED);
		}
		
		return targetPosition;
	}

	@Override
	public Boolean validateMovement(Position currentPosition, Position targetPosition) {
		// TODO Auto-generated method stub
		return null;
	}

}
