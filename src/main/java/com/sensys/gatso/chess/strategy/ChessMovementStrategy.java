package com.sensys.gatso.chess.strategy;

import com.sensys.gatso.chess.domain.Position;
import com.sensys.gatso.chess.exception.ChessCustomException;

public interface ChessMovementStrategy {
	Position move(Position currentPosition, Position targetPosition) throws ChessCustomException;
	Boolean validateMovement(Position currentPosition, Position targetPosition);
}
