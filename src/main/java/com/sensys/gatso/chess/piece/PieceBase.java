package com.sensys.gatso.chess.piece;

import com.sensys.gatso.chess.enumerator.ChessPiece;

//Class with constructor
public class PieceBase extends Piece {

	public PieceBase(ChessPiece piece) {
	    super();
		setColor(piece.getColor());
		setMovement(piece.getMovement());
		setInitialPosition(piece.getInitialPosition());
	}

}
