package com.sensys.gatso.chess.piece;

import com.sensys.gatso.chess.enumerator.ChessPiece;

public class Rook extends PieceBase {

	public Rook(ChessPiece piece) {
		super(piece);
	}

}
