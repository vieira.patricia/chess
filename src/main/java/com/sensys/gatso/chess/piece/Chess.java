package com.sensys.gatso.chess.piece;

import com.sensys.gatso.chess.domain.Board;
import com.sensys.gatso.chess.enumerator.ChessPiece;

//Chess class  - to set the chessPieces on board
public class Chess {

	private Board board;

	public Chess() {
		
		board = new Board();
		
		//Adding all white elements in the initial position
		board.getMatrix().put(ChessPiece.ROOK1_WHITE.getInitialPosition(), new Rook(ChessPiece.ROOK1_WHITE));
		board.getMatrix().put(ChessPiece.KNIGHT1_WHITE.getInitialPosition(), new Knight(ChessPiece.KNIGHT1_WHITE));
		board.getMatrix().put(ChessPiece.BISHOP1_WHITE.getInitialPosition(), new Bishop(ChessPiece.BISHOP1_WHITE));
		board.getMatrix().put(ChessPiece.KING_WHITE.getInitialPosition(), new King(ChessPiece.KING_WHITE));
		board.getMatrix().put(ChessPiece.QUEEN_WHITE.getInitialPosition(), new Queen(ChessPiece.QUEEN_WHITE));
		board.getMatrix().put(ChessPiece.BISHOP2_WHITE.getInitialPosition(), new Bishop(ChessPiece.BISHOP2_WHITE));
		board.getMatrix().put(ChessPiece.KNIGHT2_WHITE.getInitialPosition(), new Knight(ChessPiece.KNIGHT2_WHITE));
		board.getMatrix().put(ChessPiece.ROOK2_WHITE.getInitialPosition(), new Pawn(ChessPiece.ROOK2_WHITE));
		board.getMatrix().put(ChessPiece.PAWN1_WHITE.getInitialPosition(), new Pawn(ChessPiece.PAWN1_WHITE));
		board.getMatrix().put(ChessPiece.PAWN2_WHITE.getInitialPosition(), new Pawn(ChessPiece.PAWN2_WHITE));
		board.getMatrix().put(ChessPiece.PAWN3_WHITE.getInitialPosition(), new Pawn(ChessPiece.PAWN3_WHITE));
		board.getMatrix().put(ChessPiece.PAWN4_WHITE.getInitialPosition(), new Pawn(ChessPiece.PAWN4_WHITE));
		board.getMatrix().put(ChessPiece.PAWN5_WHITE.getInitialPosition(), new Pawn(ChessPiece.PAWN5_WHITE));
		board.getMatrix().put(ChessPiece.PAWN6_WHITE.getInitialPosition(), new Pawn(ChessPiece.PAWN6_WHITE));
		board.getMatrix().put(ChessPiece.PAWN7_WHITE.getInitialPosition(), new Pawn(ChessPiece.PAWN7_WHITE));
		board.getMatrix().put(ChessPiece.PAWN8_WHITE.getInitialPosition(), new Pawn(ChessPiece.PAWN8_WHITE));
			
		//Adding all black elements in the initial position
		board.getMatrix().put(ChessPiece.ROOK1_BLACK.getInitialPosition(), new Rook(ChessPiece.ROOK1_BLACK));
		board.getMatrix().put(ChessPiece.KNIGHT1_BLACK.getInitialPosition(), new Knight(ChessPiece.KNIGHT1_BLACK));
		board.getMatrix().put(ChessPiece.BISHOP1_BLACK.getInitialPosition(), new Bishop(ChessPiece.BISHOP1_BLACK));
		board.getMatrix().put(ChessPiece.KING_BLACK.getInitialPosition(), new King(ChessPiece.KING_BLACK));
		board.getMatrix().put(ChessPiece.QUEEN_BLACK.getInitialPosition(), new Queen(ChessPiece.QUEEN_BLACK));
		board.getMatrix().put(ChessPiece.BISHOP2_BLACK.getInitialPosition(), new Bishop(ChessPiece.BISHOP2_BLACK));
		board.getMatrix().put(ChessPiece.KNIGHT2_BLACK.getInitialPosition(), new Knight(ChessPiece.KNIGHT2_BLACK));
		board.getMatrix().put(ChessPiece.ROOK2_BLACK.getInitialPosition(), new Pawn(ChessPiece.ROOK2_BLACK));
		board.getMatrix().put(ChessPiece.PAWN1_BLACK.getInitialPosition(), new Pawn(ChessPiece.PAWN1_BLACK));
		board.getMatrix().put(ChessPiece.PAWN2_BLACK.getInitialPosition(), new Pawn(ChessPiece.PAWN2_BLACK));
		board.getMatrix().put(ChessPiece.PAWN3_BLACK.getInitialPosition(), new Pawn(ChessPiece.PAWN3_BLACK));
		board.getMatrix().put(ChessPiece.PAWN4_BLACK.getInitialPosition(), new Pawn(ChessPiece.PAWN4_BLACK));
		board.getMatrix().put(ChessPiece.PAWN5_BLACK.getInitialPosition(), new Pawn(ChessPiece.PAWN5_BLACK));
		board.getMatrix().put(ChessPiece.PAWN6_BLACK.getInitialPosition(), new Pawn(ChessPiece.PAWN6_BLACK));
		board.getMatrix().put(ChessPiece.PAWN7_BLACK.getInitialPosition(), new Pawn(ChessPiece.PAWN7_BLACK));
		board.getMatrix().put(ChessPiece.PAWN8_BLACK.getInitialPosition(), new Pawn(ChessPiece.PAWN8_BLACK));
		

	}

	public Board getBoard() {
		return board;
	}

}
