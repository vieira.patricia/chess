package com.sensys.gatso.chess.piece;

import com.sensys.gatso.chess.domain.Position;
import com.sensys.gatso.chess.enumerator.Color;
import com.sensys.gatso.chess.strategy.ChessMovementStrategy;

public abstract class Piece {
	//Color of the piece
	private Color color;
	//Interface movement with 2 methods to be implemented by classes
	private ChessMovementStrategy movement;	
	//Initial Position of the piece
	private Position initialPosition;

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public ChessMovementStrategy getMovement() {
		return movement;
	}

	public void setMovement(ChessMovementStrategy movement) {
		this.movement = movement;
	}

	public Position getInitialPosition() {
		return initialPosition;
	}

	public void setInitialPosition(Position initialPosition) {
		this.initialPosition = initialPosition;
	}
	
}
