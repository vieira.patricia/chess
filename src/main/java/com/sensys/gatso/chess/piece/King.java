package com.sensys.gatso.chess.piece;

import com.sensys.gatso.chess.enumerator.ChessPiece;

public class King extends PieceBase {

	public King(ChessPiece piece) {
		super(piece);
	}

}
