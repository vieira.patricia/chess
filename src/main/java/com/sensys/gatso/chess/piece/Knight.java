package com.sensys.gatso.chess.piece;

import com.sensys.gatso.chess.enumerator.ChessPiece;

public class Knight extends PieceBase {

	public Knight(ChessPiece piece) {
		super(piece);
	}

}
