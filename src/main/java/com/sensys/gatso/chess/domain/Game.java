package com.sensys.gatso.chess.domain;

import com.sensys.gatso.chess.piece.Chess;

public class Game {
	private Chess chess;	
	private Player player1;
	private Player player2;	

	public Game(Player player1, Player player2) {	
		chess = new Chess();
		this.player1 = player1;
		this.player2 = player2;		
	}

	public Chess getChess() {
		return chess;
	}

	public void setChess(Chess chess) {
		this.chess = chess;
	}

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}
	
}
