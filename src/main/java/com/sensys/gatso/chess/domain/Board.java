package com.sensys.gatso.chess.domain;

import java.util.HashMap;
import java.util.Map;

import com.sensys.gatso.chess.piece.Piece;

public class Board {
	private Map<Position, Piece> matrix;
	public static int LIMIT = 7; 
	
	public Board() {		
		matrix = new HashMap<>();
		for (int i = 0; i < LIMIT; i++) {
			for (int j = 0; j < LIMIT; j++) {	
				Position position = new Position(i, j);
				matrix.put(position, null);
			}
		}
	}

	public Map<Position, Piece> getMatrix() {
		return matrix;
	}

	public void setMatrix(Map<Position, Piece> board) {
		this.matrix = board;
	}
}
