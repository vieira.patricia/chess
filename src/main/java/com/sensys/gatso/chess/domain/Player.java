package com.sensys.gatso.chess.domain;

import com.sensys.gatso.chess.enumerator.Color;

public class Player {	
	
	private String name;
	private Color color;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}
