package com.sensys.gatso.chess.service;

import com.sensys.gatso.chess.domain.Game;
import com.sensys.gatso.chess.domain.Player;
import com.sensys.gatso.chess.domain.Position;
import com.sensys.gatso.chess.exception.ChessCustomException;
import com.sensys.gatso.chess.piece.Piece;

public class GameServiceImpl implements GameService{

	@Override
	public Game startGame(Player p1, Player p2) {
		return new Game(p1, p2);
		
	}

	@Override
	public void endGame(Game game) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Position play(Player player, Piece piece, Position currentPosition, Position targetPosition) throws ChessCustomException {
		return piece.getMovement().move(currentPosition, targetPosition);
	}

}
