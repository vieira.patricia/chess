package com.sensys.gatso.chess.service;

import com.sensys.gatso.chess.domain.Game;
import com.sensys.gatso.chess.domain.Player;
import com.sensys.gatso.chess.domain.Position;
import com.sensys.gatso.chess.exception.ChessCustomException;
import com.sensys.gatso.chess.piece.Piece;

public interface GameService {
	
	Game startGame(Player p1, Player p2);
	void endGame(Game game);
	Position play(Player player, Piece piece, Position currentPosition, Position targetPosition) throws ChessCustomException;
}
