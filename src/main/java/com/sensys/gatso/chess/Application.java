package com.sensys.gatso.chess;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.sensys.gatso.chess.domain.Game;
import com.sensys.gatso.chess.domain.Player;
import com.sensys.gatso.chess.domain.Position;
import com.sensys.gatso.chess.enumerator.Color;
import com.sensys.gatso.chess.exception.ChessCustomException;
import com.sensys.gatso.chess.piece.Piece;
import com.sensys.gatso.chess.service.GameService;
import com.sensys.gatso.chess.service.GameServiceImpl;

@SpringBootApplication
public class Application 
{
	public static void main( String[] args ) {		

		GameService gameService = new GameServiceImpl();
		Player p1 = new Player();
		Player p2 = new Player();

		int index = 0;
		Player[] currentPlayer = {p1,p2};
		while (true) {	
			p1.setColor(Color.BLACK);
			p2.setColor(Color.WHITE);
			
			Game game = gameService.startGame(p1, p2);
			
			//Simulating one movement
			Position currentPosition = new Position(0, 1);
			Position targetPosition = new Position(0, 2);
			Piece pieceToBeMoved = game.getChess().getBoard().getMatrix().get(currentPosition);	
			
			try {
				gameService.play(currentPlayer[index], pieceToBeMoved, currentPosition, targetPosition);
			} catch (ChessCustomException e) {
				//Shows an error at GUI level
				e.printStackTrace();
			}
			
			if (index == 0){
				index = 1;
			}
			else{
				index = 0;
			}
			
		}

	}
}
