package com.sensys.gatso.chess.exception;

import com.sensys.gatso.chess.enumerator.ChessException;

public class ChessCustomException extends Exception {

	private static final long serialVersionUID = -3363740115087540833L;

	private ChessException exception;
	
	public ChessCustomException(ChessException exception) {
		this.exception = exception;		
	}

	public ChessException getException() {
		return exception;
	}

}
